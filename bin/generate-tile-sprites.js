const images = require('images');
const fs = require('fs');
const path = require('path');
const minimist = require('minimist');

const args = minimist(process.argv.slice(2), {
    string: ['input', 'output'],
    default: { output: null },
    alias: { i: 'input', o: 'output' },
    '--': true,
    unknown: () => {
        console.log('Invalid arguments!');
        process.exit(1);
    },
});

const createChakanImage = (sourceImg) => {
    const WIDTH = sourceImg.width();
    const HEIGHT = sourceImg.height();

    let chakanTileImg = images(WIDTH, HEIGHT * 2);

    return chakanTileImg.draw(sourceImg, 0, 0).draw(sourceImg, 0, HEIGHT);
};

const createRedFiveChakanImage = (normalFiveImg, redFiveImg) => {
    let chakanTileImg = images(Math.max(normalFiveImg.width(), redFiveImg.width()), normalFiveImg.height() + redFiveImg.height());

    return chakanTileImg.draw(redFiveImg, 0, 0).draw(normalFiveImg, 0, redFiveImg.height());
}


let sourceDir = args.i, outputDir = args.o;

if (!outputDir) {
    outputDir = sourceDir;
}

// Back sprite
fs.writeFileSync(path.join(outputDir, 'b.png'), fs.readFileSync(path.join(sourceDir, 'j9.png')));

// Normal suits
for (let i = 1; i <= 9; i++) {
    fs.writeFileSync(path.join(outputDir, i + 'm.png'), fs.readFileSync(path.join(sourceDir, 'm' + i + '.png')));
    fs.writeFileSync(path.join(outputDir, i + 'p.png'), fs.readFileSync(path.join(sourceDir, 'p' + i + '.png')));
    fs.writeFileSync(path.join(outputDir, i + 's.png'), fs.readFileSync(path.join(sourceDir, 's' + i + '.png')));
    fs.writeFileSync(path.join(outputDir, i + 'm_.png'), fs.readFileSync(path.join(sourceDir, '2m' + i + '.png')));
    fs.writeFileSync(path.join(outputDir, i + 'p_.png'), fs.readFileSync(path.join(sourceDir, '2p' + i + '.png')));
    fs.writeFileSync(path.join(outputDir, i + 's_.png'), fs.readFileSync(path.join(sourceDir, '2s' + i + '.png')));
}

// Honor tiles
for (let i = 1; i <= 7; i++) {
    fs.writeFileSync(path.join(outputDir, i + 'z.png'), fs.readFileSync(path.join(sourceDir, 'j' + i + '.png')));
    fs.writeFileSync(path.join(outputDir, i + 'z_.png'), fs.readFileSync(path.join(sourceDir, '2j' + i + '.png')));
}


// Red fives
fs.writeFileSync(path.join(outputDir, '0m.png'), fs.readFileSync(path.join(sourceDir, 'me.png')));
fs.writeFileSync(path.join(outputDir, '0p.png'), fs.readFileSync(path.join(sourceDir, 'pe.png')));
fs.writeFileSync(path.join(outputDir, '0s.png'), fs.readFileSync(path.join(sourceDir, 'se.png')));
fs.writeFileSync(path.join(outputDir, '0m_.png'), fs.readFileSync(path.join(sourceDir, '2me.png')));
fs.writeFileSync(path.join(outputDir, '0p_.png'), fs.readFileSync(path.join(sourceDir, '2pe.png')));
fs.writeFileSync(path.join(outputDir, '0s_.png'), fs.readFileSync(path.join(sourceDir, '2se.png')));

// Chakan tiles
for (suit of ['m', 'p', 's']) {
    for (let i = 1; i <= 9; i++) {
        const sourceImg = images(path.join(sourceDir, '2' + suit + i + '.png'));
        const chakanImg = createChakanImage(sourceImg);
        chakanImg.save(path.join(outputDir, i + suit + '_' + i + suit + '_' + '.png'));

        if (i === 5) {
            const redFiveImg = images(path.join(sourceDir, '2' + suit + 'e' + '.png'));
            const redFiveChakanImg = createRedFiveChakanImage(sourceImg, redFiveImg);
            redFiveChakanImg.save(path.join(outputDir, '0' + suit + '_' + i + suit + '_' + '.png'));
        }
    }
}

for (let i = 1; i <= 7; i++) {
    const sourceImg = images(path.join(sourceDir, '2' + 'j' + i + '.png'));
    const chakanImg = createChakanImage(sourceImg);
    chakanImg.save(path.join(outputDir, i + 'z' + '_' + i + 'z' + '_' + '.png'));
}
