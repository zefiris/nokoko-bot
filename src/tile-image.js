const images = require('images');
const path = require('path');

// Create a padding 8 pixels wide
const PADDING = images(8, 1);

// Directory to the image tiles
const IMG_DIR = 'data/img/tilesets';

// Folder name of the tilesets
const TileSets = ['wiki', 'normal2', 'meirio2'];

module.exports.TileSets = TileSets.slice();

module.exports.getTileImage = (tileStr, tileSet) => {
    const tileGroups = _parseTileString(tileStr);
    if (!tileSet) {
        tileSet = TileSets[2];
    }
    if (tileGroups) {
        let totalWidth = 0, totalHeight = 0;
        const imageList = [];

        imageList.push(PADDING);
        totalWidth += PADDING.width();

        for (tileGroup of tileGroups) {
            for (tile of tileGroup) {
                let tileImage = images(path.join(IMG_DIR, tileSet, tile + '.png'));
                imageList.push(tileImage);
                totalWidth += tileImage.width();
                if (tileImage.height() > totalHeight) {
                    totalHeight = tileImage.height();
                }
            }
            imageList.push(PADDING);
            totalWidth += PADDING.width();
        }

        let finalTileImage = images(totalWidth, totalHeight);
        let currentX = 0;
        for (image of imageList) {
            finalTileImage = finalTileImage.draw(image, currentX, totalHeight - image.height());
            currentX += image.width();
        }

        return finalTileImage.encode('png');
    }

    return null;
};

const _parseTileString = (tileStr) => {
     // Split tiles into groups
    const rawGroups = tileStr.trim().split(' ');
    if (rawGroups.length > 5) {
        return null;
    }

    let tileGroups = [];
    for (let rawGroup of rawGroups) {
        // Convert the rawGroup to a char array
        let charArray = Array.from(rawGroup),
            tiles = [];

        // Temporary variables
        let isFuuro = false,
            isSideWayTile = false,
            suit = undefined;

        // Expect at least 2 characters
        if (charArray.length < 1) {
            return null;
        }

        // Begin processing the tile group
        while (charArray.length > 0) {
            if (tileGroups.length > 0) {
                // Fuuro if tilesGroup already contains a group
                isFuuro = true;
            }
            let char = charArray.pop();
            if (char === 'm' || char === 'p' || char === 's' || char === 'z') {
                suit = char;
            } else if (char === '\'') {
                if (isSideWayTile) {
                    // Should not have two consecutive ' in the string
                    return null;
                }
                isFuuro = true;
                isSideWayTile = true;
            } else if (!isNaN(char)) {
                if (!suit) {
                    // Did not specify suit
                    return null;
                }
                if (isSideWayTile) {
                    tiles.splice(0, 0, char + suit + '_');
                } else {
                    tiles.splice(0, 0, char + suit);
                }

                // Reset sideway tile to false for next tile
                isSideWayTile = false;
            } else {
                // Invalid input
                return null;
            }

            if (!isFuuro && tiles.length > 14) {
                // A hand cannot have more than 14 tiles
                return null;
            } else if (isFuuro && tiles.length > 4) {
                // A call cannot have more than four tiles
                return null;
            }
        }

        // Flip the first and last tile if tile string starts with ' (ankou)
        if (isSideWayTile) {
            tiles[0] = 'b';
            tiles[tiles.length - 1] = 'b';
        }

        // Convert two consecutive sideway tiles to Chakan
        for (let i = 1; i < tiles.length; i++) {
            let previousTile = tiles[i - 1];
            let thisTile = tiles[i];
            if (thisTile.charAt(thisTile.length - 1) === '_' && previousTile === thisTile) {
                let newTile = previousTile + thisTile;
                tiles.splice(i - 1, 2, newTile);
            } else if (thisTile === '5m_' && previousTile === '0m_' || thisTile === '0m_' && previousTile === '5m_') {
                let newTile = '0m_5m_';
                tiles.splice(i - 1, 2, newTile);
            } else if (thisTile === '5p_' && previousTile === '0p_' || thisTile === '0p_' && previousTile === '5p_') {
                let newTile = '0p_5p_';
                tiles.splice(i - 1, 2, newTile);
            } else if (thisTile === '5s_' && previousTile === '0s_' || thisTile === '0s_' && previousTile === '5s_') {
                let newTile = '0s_5s_';
                tiles.splice(i - 1, 2, newTile);
            }
        }

        // Push the tiles to tile group array
        tileGroups.push(tiles);
    }

    return tileGroups;
};
