module.exports.GenericCommandLibrary = Object.freeze({
    COMMAND_PREFIX: '.',
    COMMANDS: Object.freeze({
        CALC: ['calc', 'calculator'],
        CHAKAN: ['chakan', 'kakan'],
        CHART: ['chart', 'table'],
        CHII: ['chi', 'chii'],
        CHOOSE: ['choose', 'pick'],
        CMD: ['cmd', 'command', 'commands', 'cmds', 'usage'],
        DEFINE: ['define', 'dict'],
        ECHO: ['echo'],
        FORUM: ['forum', 'forums'],
        GUIDE: ['guide', 'guides'],
        HELP: ['help', 'h', '?'],
        KAN: ['kan'],
        PON: ['pon'],
        ROLL: ['roll'],
        RON: ['ron'],
        SAY: ['say'],
        WIKI: ['wiki'],
        YAKU: ['yaku', 'yakus'],
        YAKUMAN: ['yakuman'],
    }),
});
