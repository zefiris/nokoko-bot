// 3rd Party Libraries
const discord = require('discord.js');
const client = new discord.Client();

// Imports
const GenericCommandLibrary = require('./constants').GenericCommandLibrary;
const RawStrings = require('../data/strings.json');
const TileImage = require('./tile-image');

client.on('ready', () => {
    console.log('I am ready!');
});

client.on('message', (m) => {
    // Start a promise chain
    Promise.resolve().then(() => {
        if (m.author.bot) {
            // Ignore messages sent from bots
            return Promise.resolve();
        }

        // Handle commands
        let command = parseCommand(m.content, GenericCommandLibrary);

        if (command) {
            if (command.name === 'CALC') {
                // Post the link to the mahjong score calculator
                return m.channel.sendMessage(RawStrings.calc);
            } else if (command.name === 'CHART') {
                return m.channel.sendMessage(RawStrings.chart);
            } else if (command.name === 'CHOOSE' && command.content && command.content !== '') {
                // Choose a thing from a list of things
                const delimiter = '|';
                let choices = command.content.split(delimiter);

                // Remove choices that have length 0
                choices = choices.filter((choice) => {
                    return choice.length > 0;
                });

                const randomIndex = Math.floor(Math.random() * choices.length);

                return m.reply(choices[randomIndex]);
            } else if (command.name === 'ECHO' && command.content && command.content !== '') {
                // Repeat whatever the author said (mention the person)
                return m.reply(command.content);
            } else if (command.name === 'FORUM') {
                // Post the link to the forum
                return m.channel.sendMessage(RawStrings.forum);
            } else if (command.name === 'HELP') {
                // Show the help message
                return m.channel.sendMessage(RawStrings.help);
            } else if (command.name === 'GUIDE') {
                // Post the link to mahjong guide
                return m.channel.sendMessage(RawStrings.guide);
            } else if (command.name === 'SAY') {
                // Repeat whatever the author said
                return m.channel.sendMessage(command.content);
            } else if (command.name === 'WIKI') {
                // Post the link to arcturus' wiki
                return m.channel.sendMessage(RawStrings.wiki);
            }
        }

        // Check for tile string
        return Promise.all(getSendTileImagePromises(m.content, m.channel));
    })
    .catch((err) => {
        console.log(err.stack);
    });
});

// Given a message string and command library, returns an object containing { name, content } or null
const parseCommand = (messageContent, commandLibrary) => {
    // Generic command handling
    for (let commandName in commandLibrary.COMMANDS) {
        const found = commandLibrary.COMMANDS[commandName].some((value) => {
            return messageContent.split(' ')[0].toLowerCase() === commandLibrary.COMMAND_PREFIX + value;
        });
        if (found) {
            return {
                name: commandName,
                content: messageContent.substring(messageContent.split(' ')[0].length + 1),
            };
        }
    }

    return null;
};

// Construct an array of promises for tile images given a text message
const getSendTileImagePromises = (testString, channel) => {
    // Check for tile string
    const tileStringPattern = /\[([0-9mpsz' ]+)\]/i;
    const sendTileImagePromises = [];

    while (tileStringPattern.exec(testString)) {
        let match = tileStringPattern.exec(testString)[1];
        testString = testString.replace(match, '');

        const tileImage = TileImage.getTileImage(match.trim());
        if (tileImage) {
            sendTileImagePromises.push(channel.sendFile(tileImage));
        }
    }

    return sendTileImagePromises;
}

// TODO: Hide this token and generate a new one
client.login('MjkxNTk4MTM3MDc0NjQ3MDQw.C7Lghg.50q9jPcxjwnHGIEFXRqtHz0FknM');
